import requests
import settings


class ClientException(Exception):
    def __init__(self, message, response=None):
        super(ClientException, self).__init__(message)
        self.response = response


class AuthorizationExcetion(ClientException):
    pass


class BadRequestExcetion(ClientException):
    pass


class Client(object):
    def signup(self, username, email, password):
        return self._make_post_request(
            relative_url='api/signup',
            data={
                'username': username,
                'email': email,
                'password': password
            }
        )

    def login(self, username, password):
        return self._make_post_request(
            relative_url='api/login',
            data={
                'username': username,
                'password': password
            }
        ).get('token', None)

    def create_post(self, name, content, token):
        return self._make_post_request(
            relative_url='api/posts',
            data={
                'name': name,
                'content': content
            },
            token=token
        )

    def like_post(self, post_id, token):
        return self._make_post_request(
            relative_url='api/posts/{}/like'.format(post_id),
            token=token
        )

    def _make_get_request(self, relative_url, token=None):
        return self._make_request(
            function=requests.get,
            relative_url=relative_url,
            token=token
        ) 

    def _make_post_request(self, relative_url, data=None, token=None):
        return self._make_request(
            function=requests.post,
            relative_url=relative_url,
            data=data,
            token=token
        )

    def _make_request(self, function, relative_url, data=None, token=None):
        url = settings.SITE_ROOT_URL + relative_url

        kwargs = {}
        if data:
            kwargs['data'] = data
        if token:
            kwargs['headers'] = {'Authorization': 'JWT {}'.format(token)}

        response = function(url, **kwargs)

        if token and response.status_code == 403:
            raise AuthorizationExcetion('Authorization Failed', response)
        if response.status_code == 400:
            raise BadRequestExcetion('Incorrect Data', response)
        if response.status_code < 200 or response.status_code >= 300:
            raise ClientException('Something wrong with API', response)

        return response.json()
