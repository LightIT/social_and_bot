import settings
from faker import Faker
from services import SimpleService


def generate_users():
    faker = Faker()
    while True:
        yield {
            'username': faker.user_name(),
            'email': faker.free_email(),
            'password': faker.password()
        }

def generate_posts():
    faker = Faker()
    while True:
        yield {
            'name': faker.sentence(2),
            'content': faker.text(450)
        }


class Bot(object):
    def __init__(self, service):
        self.service = service

    def run(self):
        print 'Start create users'
        users = self.service.create_users(
            user_iterable=generate_users(),
            max_users=settings.NUMBER_OF_USERS,
        )
        print 'Start create posts'
        posts = self.service.create_posts(
            post_iterable=generate_posts(),
            users=users,
            max_posts_per_user=settings.MAX_POSTS_PER_USER
        )
        print 'Start create likes'
        self.service.create_likes(
            users=users,
            posts=posts,
            max_likes_per_user=settings.MAX_LIKES_PER_USER
        )
        print 'Finished'


if __name__ == '__main__':
    bot = Bot(SimpleService())
    bot.run()
