import random
import copy
from client import Client, AuthorizationExcetion, BadRequestExcetion, ClientException


class SimpleService(object):
    def __init__(self):
        self.client = Client()

    def create_users(self, user_iterable, max_users):
        users = []
        for data in user_iterable:
            user = None
            try:
                user = self.client.signup(**data)
            except BadRequestExcetion:
                continue
            user['password'] = data['password']
            users.append(user)

            if len(users) == max_users:
                return users

    def create_posts(self, post_iterable, users, max_posts_per_user):
        posts = []
        for user in users:
            max_posts = random.randint(1, max_posts_per_user)
            token = self.client.login(user['username'], user['password'])
            post_count = 0
            for data in post_iterable:
                post = None
                try:
                    post = self.client.create_post(token=token, **data)
                except AuthorizationExcetion:
                    token = self.client.login(user['username'], user['password'])
                    continue
                posts.append(post)
                post_count += 1
                if post_count == max_posts:
                    break
        return posts

    def create_likes(self, users, posts, max_likes_per_user):
        users = copy.deepcopy(users)
        posts = copy.deepcopy(posts)
        users = sorted(users, key=lambda u: self._get_user_post_count(u, posts))
        for user in reversed(users):
            user['liked_post_ids'] = []
            token = self.client.login(user['username'], user['password'])
            while len(user['liked_post_ids']) < max_likes_per_user:
                likeable_posts = self._get_user_likeable_posts(user, posts)
                if not len(likeable_posts):
                    break
                post = random.choice(likeable_posts)
                try:
                    self.client.like_post(post_id=post['id'], token=token)
                except AuthorizationExcetion:
                    token = self.client.login(user['username'], user['password'])
                    continue
                except ClientException:
                    continue
                post['like_count'] += 1
                user['liked_post_ids'].append(post['id'])
                posts = self._get_likeable_posts(posts)

    def _get_user_post_count(self, user, posts):
        result = 0
        for post in posts:
            if post['user']['id'] == user['id']:
                result += 1

    def _get_likeable_posts(self, posts):
        likeable_user_ids = set()
        for post in posts:
            if post['like_count'] == 0:
                likeable_user_ids.add(post['user']['id'])
        return [post for post in posts if post['user']['id'] in likeable_user_ids]

    def _get_user_likeable_posts(self, user, posts):
        result = []
        for post in posts:
            if post['user']['id'] == user['id']:
                continue
            if post['id'] in user['liked_post_ids']:
                continue
            result.append(post)
        return result
