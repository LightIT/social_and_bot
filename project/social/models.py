from django.conf import settings
from django.db import models
from jsonfield import JSONField
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from tasks import update_user


class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=70)
    content = models.CharField(max_length=500)
    created_date = models.DateTimeField(auto_now_add=True)
    likes = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name='user_likes'
    )

    class Meta:
        db_table = 'posts'

    def __unicode__(self):
        return self.name


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def user_created(instance, created, *args, **kwargs):
    if not created:
        return None

    update_user.apply_async(kwargs={'pk': instance.id})
