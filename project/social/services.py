import urllib
import requests
import clearbit
from django.conf import settings


clearbit.key = settings.CLEARBIT_API_KEY


class EmailhunterService(object):
    MIN_VALID_SCORE = 50

    def __init__(self, email):
        self.email = email
        self.data = None
        self._run_request()

    def is_valid(self):
        if self.data is None:
            return False
        return self.data.get('data', {}).get('score', 0) >= self.MIN_VALID_SCORE
    
    def _run_request(self):
        query_string = urllib.urlencode({
            'email': self.email,
            'api_key': settings.EMAILHUNTER_API_KEY
        })
        response = requests.get(
            'https://api.hunter.io/v2/email-verifier?{}'.format(query_string)
        )
        self.data = response.json()


class ClearbitService(object):
    def __init__(self, email):
        self.email = email
        self.data = None
        self._run_request()

    def get_first_name(self):
        try:
            return self.data.get('person', {}).get('name', {}).get('givenName', None)
        except AttributeError:
            return None

    def get_last_name(self):
        try:
            return self.data.get('person', {}).get('name', {}).get('familyName', None)
        except AttributeError:
            return None

    def _run_request(self):
        data = clearbit.Enrichment.find(email=self.email, stream=True)
        if data:
            self.data = dict(data)
