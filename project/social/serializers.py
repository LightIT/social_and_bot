from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from django.contrib.auth.models import User
from services import EmailhunterService
from . import models


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}
        validators = [
            UniqueTogetherValidator(
                queryset=User.objects.all(),
                fields=('email',)
            )
        ]

    def validate(sejf, data):
        email = data.get('email', None)

        if not EmailhunterService(email).is_valid():
            raise serializers.ValidationError({'email': "Email address is not approved."})

        return data

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class PostSerializer(serializers.ModelSerializer):
    like_count = serializers.IntegerField(source='likes.count', read_only=True)
    user = UserSerializer(default=serializers.CurrentUserDefault())

    class Meta:
        model = models.Post
        exclude = ('likes',)
        extra_kwargs = {
            'created_date': {'read_only': True}
        }
