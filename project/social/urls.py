from django.conf.urls import include, url
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from rest_framework.routers import SimpleRouter
from . import views


router = SimpleRouter(trailing_slash=False)
router.register(r'api/posts', views.PostViewSet,)

urlpatterns = [
    url(r'^api/signup', views.UserSignupView.as_view()),
    url(r'^api/login', obtain_jwt_token),
    url(r'^api/token-refresh', refresh_jwt_token),
    url(r'^', include(router.urls)),
]
