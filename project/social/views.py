from django.contrib.auth import get_user_model
from django.db import IntegrityError
from rest_framework import status, viewsets
from rest_framework.decorators import api_view, permission_classes, detail_route
from rest_framework.generics import CreateAPIView, DestroyAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from permissions import IsOwnerOrReadOnly
from . import models
from . import serializers


class UserSignupView(CreateAPIView):
    permission_classes = (AllowAny,)
    queryset = get_user_model().objects.all()
    serializer_class = serializers.UserSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = models.Post.objects.all().select_related('user')
    serializer_class = serializers.PostSerializer
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly)

    @detail_route(methods=['post'])
    def like(self, request, pk):
        return self._like_or_unlike(request, pk, is_like=True)

    @detail_route(methods=['post'])
    def unlike(self, request, pk):
        return self._like_or_unlike(request, pk, is_like=False)

    def _like_or_unlike(self, request, pk, is_like=True):
        post = None
        try:
            post = models.Post.objects.get(pk=int(pk))
        except models.Post.DoesNotExist:
            return Response({'message': 'Post does not exist.'}, status=status.HTTP_404_NOT_FOUND)

        if is_like:
            post.likes.add(request.user)
            return Response({'message': 'liked'}, status=status.HTTP_200_OK)
        else:
            post.likes.remove(request.user)
            return Response({'message': 'unliked'}, status=status.HTTP_200_OK)
