from celery.task import task
from django.contrib.auth.models import User
from services import ClearbitService


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def update_user(pk):
	user = None
	try:
		user = User.objects.get(pk=pk)
	except User.DoesNotExist:
		return None

	service = ClearbitService(user.email)
	first_name = service.get_first_name()
	last_name = service.get_last_name()

	if first_name:
		user.first_name = first_name

	if last_name:
		user.last_name = last_name

	user.save()
