from django.contrib import admin
from django.contrib.auth.models import Group
from . import models


class PostAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.unregister(Group)
admin.site.register(models.Post, PostAdmin)