## Project
folder: project/
### Install
```
pip install -r requirements.txt
./manage.py migrate
./manage.py loaddata users
```
### Settings
file: project/settings.py

* EMAILHUNTER_API_KEY - key for emailhunter.co
* CLEARBIT_API_KEY - key for clearbit.com 
### Run
```
./manage.py runserver
celery -A project worker -l info
```
## Bot
folder: bot/
### Install
```
pip install -r requirements.txt
```
### Settings 
file: settings.py

* SITE_ROOT_URL - root url of site
### Run
```
python bot.py
```